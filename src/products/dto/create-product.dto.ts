import { IsNotEmpty, MinLength, IsPositive } from 'class-validator';

export class CreateProductDto {
  @MinLength(10)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
